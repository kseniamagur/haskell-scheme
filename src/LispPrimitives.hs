{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts#-}
module LispPrimitives(
    primitives
) where

import Control.Monad
import Control.Monad.Except

import LispVal

primitives :: [(String , [LispVal] -> ThrowsError LispVal)]
primitives = [("+" , numericBinop (+)),
                ("-" , numericBinop (-)),
                ("*" , numericBinop (*)),
                ("/" , numericBinop div),
                ("mod" , numericBinop mod),
                ("quotient" , numericBinop quot),
                ("remainder" , numericBinop rem),
                ("=" , numBoolBinop (==)),
                ("<" , numBoolBinop (<)),
                (">" , numBoolBinop (>)),
                ("/=" , numBoolBinop (/=)),
                (">=" , numBoolBinop (>=)),
                ("<=" , numBoolBinop (<=)),
                ("&&" , boolBoolBinop (&&)),
                ("||" , boolBoolBinop (||)) ,
                ("string=?" , strBoolBinop (==)),
                ("string>?" , strBoolBinop (>)),
                ("string<?" , strBoolBinop (<)),
                ("string<=?" , strBoolBinop (<=)),
                ("string>=?" , strBoolBinop (>=)),
                ("string-length", strLength),
                ("string-append",strAppend),
                ("string", charsToString),
                ("car" , car),
                ("cdr" , cdr),
                ("cons" , cons),
                ("eq?" , eqv),
                ("eqv?" , eqv),
                ("equal?" , equal),
                ("string?", isString),
                ("char?", isChar),
                ("number?", isNumber),
                ("boolean?", isBool)]
                
numericBinop :: (Integer -> Integer -> Integer) -> [LispVal] -> ThrowsError LispVal
numericBinop op singleVal@[_] = throwError $ NumArgs 2 singleVal
numericBinop op params = mapM unpackNum params >>= return . Number . foldl1 op

boolBinop :: (LispVal -> ThrowsError a) -> (a -> a -> Bool) -> [LispVal] -> ThrowsError LispVal
boolBinop unpacker op args = if length args /= 2
                                then throwError $ NumArgs 2 args
                                else do left <- unpacker $ args !! 0
                                        right <- unpacker $ args !! 1
                                        return $ Bool $ left `op` right

numBoolBinop = boolBinop unpackNum
strBoolBinop = boolBinop unpackStr
boolBoolBinop = boolBinop unpackBool

unpackNum :: LispVal -> ThrowsError Integer
unpackNum (Number n) = return n
unpackNum (String n) = let parsed = reads n in
                            if null parsed
                            then throwError $ TypeMismatch "number" $ String n
                            else return $ fst $ parsed !! 0
unpackNum (List [n]) = unpackNum n
unpackNum notNum = throwError $ TypeMismatch "number" notNum

unpackStr :: LispVal -> ThrowsError String
unpackStr (String s) = return s
unpackStr (Number s) = return $ show s
unpackStr (Bool s) = return $ show s
unpackStr notString = throwError $ TypeMismatch "string" notString

unpackChar:: LispVal -> ThrowsError Char
unpackChar (Character ch) = return ch
unpackChar notChar = throwError $ TypeMismatch "character" notChar

unpackBool :: LispVal -> ThrowsError Bool
unpackBool (Bool b) = return b
unpackBool notBool = throwError $ TypeMismatch "boolean" notBool

car :: [LispVal] -> ThrowsError LispVal
car [List (x : xs)] = return x
car [DottedList (x : xs) _] = return x
car [badArg] = throwError $ TypeMismatch "pair" badArg
car badArgList = throwError $ NumArgs 1 badArgList

cdr :: [LispVal] -> ThrowsError LispVal
cdr [List (x : xs)] = return $ List xs
cdr [DottedList [xs] x] = return x
cdr [DottedList (_ : xs) x] = return $ DottedList xs x
cdr [badArg] = throwError $ TypeMismatch "pair" badArg
cdr badArgList = throwError $ NumArgs 1 badArgList

cons :: [LispVal] -> ThrowsError LispVal
cons [x1, List []] = return $ List [x1]
cons [x, List xs] = return $ List $ [x] ++ xs
cons [x, DottedList xs xlast] = return $ DottedList ([x] ++ xs) xlast
cons [x1, x2] = return $ DottedList [x1] x2
cons badArgList = throwError $ NumArgs 2 badArgList


eqvList:: ([LispVal] -> ThrowsError LispVal) -> [LispVal] -> [LispVal] -> ThrowsError LispVal
eqvList comparer arg1 arg2 = return $ Bool $ (length arg1 == length arg2) && (and $ map (eqvPair comparer) $ zip arg1 arg2)
                    where eqvPair compr (x1, x2) = case compr [x1, x2] of
                                                    Left err -> False
                                                    Right (Bool val) -> val

toList:: LispVal -> LispVal
toList (DottedList xs x) = List $ xs ++ [x]

eqv :: [LispVal] -> ThrowsError LispVal
eqv [(Bool arg1), (Bool arg2)] = return $ Bool $ arg1 == arg2
eqv [(Number arg1), (Number arg2)] = return $ Bool $ arg1 == arg2
eqv [(String arg1), (String arg2)] = return $ Bool $ arg1 == arg2
eqv [(Atom arg1), (Atom arg2)] = return $ Bool $ arg1 == arg2
eqv [arg1@(DottedList xs x), arg2@(DottedList ys y)] = eqv [toList arg1, toList arg2]
eqv [(List arg1), (List arg2)] = eqvList eqv arg1 arg2
eqv [_,_] = return $ Bool False
eqv badArgList = throwError $ NumArgs 2 badArgList

data Unpacker = forall a. Eq a => AnyUnpacker (LispVal -> ThrowsError a)


unpackEquals :: LispVal -> LispVal -> Unpacker -> ThrowsError Bool
unpackEquals arg1 arg2 (AnyUnpacker unpacker) = do 
                unpacked1 <- unpacker arg1
                unpacked2 <- unpacker arg2
                return $ unpacked1 == unpacked2 
            `catchError` (const $ return False)
        
equal :: [LispVal] -> ThrowsError LispVal
equal [arg1@(DottedList xs x), arg2@(DottedList ys y)] = equal [toList arg1, toList arg2]
equal [arg1@(List _), arg2@(DottedList ys y)] =  equal [arg1 , toList arg2]
equal [arg1@(DottedList xs x), arg2@(List _)] = equal [toList arg1, arg2]
equal [(List arg1), (List arg2)] = eqvList equal arg1 arg2
equal [arg1, arg2] = do
    primitiveEquals <- liftM or $ mapM (unpackEquals arg1 arg2) [AnyUnpacker unpackNum, AnyUnpacker unpackStr, AnyUnpacker unpackBool, AnyUnpacker unpackChar]
    eqvEquals <- eqv [arg1, arg2]
    return $ Bool $ (primitiveEquals || let (Bool x) = eqvEquals in x)
equal badArgList = throwError $ NumArgs 2 badArgList


isString:: [LispVal] -> ThrowsError LispVal
isString [String _] = return $ Bool True
isString [_] = return $ Bool False
isString badArgList  = throwError $ NumArgs 1 badArgList

isBool::[LispVal] -> ThrowsError LispVal
isBool [Bool _] = return $ Bool True
isBool [_] = return $ Bool False
isBool badArgList  = throwError $ NumArgs 1 badArgList

isNumber::[LispVal] -> ThrowsError LispVal
isNumber [Number _] = return $ Bool True
isNumber [_] = return $ Bool False
isNumber badArgList  = throwError $ NumArgs 1 badArgList

isChar::[LispVal] -> ThrowsError LispVal
isChar [Character _] = return $ Bool True
isChar [_] = return $ Bool False
isChar badArgList  = throwError $ NumArgs 1 badArgList

charsToString::[LispVal] -> ThrowsError LispVal
charsToString chars = charsToStringRec chars ""

charsToStringRec ((Character ch):chars) strBase =  charsToStringRec chars (strBase ++ [ch])
charsToStringRec (notChar:_) _= throwError $ TypeMismatch "character" notChar
charsToStringRec [] strBase = return $ String strBase

strLength::[LispVal] -> ThrowsError LispVal
strLength [String str] = return $ Number (fromIntegral $ length str)
strLength [notString] = throwError $ TypeMismatch "string" notString
strLength badArgList = throwError $ NumArgs 1 badArgList

strAppend::[LispVal] -> ThrowsError LispVal
strAppend strs = strAppendRec strs ""

strAppendRec ((String str):strs) strBase = strAppendRec strs (strBase ++ str)
strAppendRec (notString:_) _ =  throwError $ TypeMismatch "string" notString
strAppendRec [] strBase = return $ String strBase