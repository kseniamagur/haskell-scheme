
module LispVal (
    LispVal(..),
    unwordsList,
    --lisp error functins
    LispError(..),
    ThrowsError(..),
    IOThrowsError(..),
    catchIOExceptionOrThrow,
    runIOThrows,
    -- env
    Env(..)
    ) where


import Control.Exception
import Control.Monad
import Control.Monad.Except
import System.IO
import Data.IORef
import Text.ParserCombinators.Parsec hiding (spaces)

type Env = IORef [(String, IORef LispVal)]

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Character Char
             | Bool Bool
             | Port Handle
             | PrimitiveFunc ([LispVal] -> ThrowsError LispVal)
             | IOFunc ([LispVal] -> IOThrowsError LispVal)
             | Func {params :: [String] , vararg :: (Maybe String),
                        body :: [LispVal] , closure :: Env}

showVal :: LispVal -> String
showVal (String contents) = "\"" ++ contents ++ "\""
showVal (Character ch) = "#\\" ++ [ch]
showVal (Atom name) = name
showVal (Number contents) = show contents
showVal (Bool True) = "#t"
showVal (Bool False ) = "#f"
showVal (List contents) = "(" ++ unwordsList contents ++ ")"
showVal (DottedList head tail) = "(" ++ unwordsList head ++ " . " ++ showVal tail ++ ")"
showVal (Port _) = "<IO port>"
showVal (IOFunc _) = "<IO primitive>"
showVal (PrimitiveFunc _) = "<primitive>"
showVal (Func {params = args , vararg = varargs , body = body, closure = env}) =
            "(lambda (" ++ unwords (map show args) ++
                (case varargs of
                    Nothing -> ""
                    Just arg -> " . " ++ arg) ++ ") ...)"

unwordsList :: [LispVal] -> String
unwordsList = unwords . map showVal

instance Show LispVal where show = showVal

-- make LispVal instance of Eq
eqLispVal:: LispVal -> LispVal -> Bool
eqLispVal (Atom x) (Atom y) = x==y
eqLispVal (List []) (List []) = True
eqLispVal (List list1) (List list2) = eqLispValList list1 list2
eqLispVal (DottedList l1 x1) (DottedList l2 x2) = (eqLispValList l1 l2) && (eqLispVal x1 x2)
eqLispVal (Number x) (Number y) = x==y
eqLispVal (String x) (String y) = x==y
eqLispVal (Character x) (Character y) = x==y
eqLispVal (Bool x) (Bool y) = x==y
eqLispVal (Port x) (Port y) = x==y
eqLispVal io1@(IOFunc _) io2@(IOFunc _) = (show io1) == (show io2)
eqLispVal pf1@(PrimitiveFunc _) pf2@(PrimitiveFunc _) = (show pf1) == (show pf2)
eqLispVal f1@(Func _ _ _ _) f2@(Func _ _ _ _) =  (show f1) == (show f2)
eqLispVal _ _ = False

eqLispValList [] [] = True
eqLispValList [] _ = False
eqLispValList _ [] = False
eqLispValList (h1:t1) (h2:t2) = (eqLispVal h1 h2) && (eqLispValList t1 t2)

instance Eq LispVal where 
    x == y = eqLispVal x y


-- added IOError for IO exceptions
data LispError = NumArgs Integer [LispVal]
               | TypeMismatch String LispVal
               | Parser ParseError
               | BadSpecialForm String LispVal
               | NotFunction String String
               | UnboundVar String String
               | IOError SomeException
               | Default String


showError :: LispError -> String
showError (UnboundVar message varname) ="UnboundVar error :: " ++ message ++ ": " ++ varname
showError (BadSpecialForm message form) ="BadSpecialForm error :: " ++ message ++ ": " ++ show form
showError (NotFunction message func) = "NotFunction error :: " ++ message ++ ": " ++ show func
showError (NumArgs expected found) = "NumArgs error :: Expected " ++ show expected ++ " args: found values " ++ unwordsList found
showError (TypeMismatch expected found) = "TypeMismatch error :: Invalid type:expected " ++ expected ++ ", found " ++ show found
showError (Parser parseErr) = "Parse error ::  " ++ show parseErr
showError (IOError someException) = "IOError error :: " ++ show someException
showError (Default msg) = "Default error :: " ++ msg

instance Show LispError where show = showError

compareLispError:: LispError -> LispError -> Bool
compareLispError (NumArgs n1 l1) (NumArgs n2 l2) = (n1==n2) && (l1==l2) 
compareLispError (TypeMismatch s1 lv1) (TypeMismatch s2 lv2) = (s1==s2) && (lv1==lv2)
compareLispError (Parser perr1) (Parser perr2) = perr1 == perr2
compareLispError (BadSpecialForm s1 lv1) (BadSpecialForm s2 lv2) = (s1==s2) && (lv1==lv2)
compareLispError (NotFunction s1 ss1) (NotFunction s2 ss2) = (s1==s2) && (ss1==ss2)
compareLispError (UnboundVar s1 ss1) (UnboundVar s2 ss2) = (s1==s2) && (ss1==ss2)
compareLispError (IOError err1) (IOError err2) = (show err1) == (show err2)
compareLispError (Default s1) (Default s2) = s1==s2
compareLispError _ _ = False

instance Eq LispError where 
    x == y = compareLispError x y


-- throwable LispError

-- for all errors except of IO
type ThrowsError = Either LispError
-- for IO errors
type IOThrowsError = ExceptT LispError IO

extractValue :: ThrowsError a -> a
extractValue (Right val) = val

-- To catch all errors except of IO 
trapError action = catchError action (return . show)

runIOThrows :: IOThrowsError String -> IO String
runIOThrows action = runExceptT (trapError action) >>= return . extractValue

-- To catch IO exceptions
catchIOExceptionOrThrow :: (a -> IO b) -> a -> IOThrowsError b 
catchIOExceptionOrThrow action input = ExceptT (do result <- Control.Exception.try (action input)
                                                   case result of
                                                     Left err -> return $ Left (IOError err)
                                                     Right res -> return $ Right res)