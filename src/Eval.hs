module Eval(
    eval,
    applyProc,
    -- env
    primitiveBindings,
    makeNormalFunc,
    makeVarargs
) where

import System.IO
import Control.Monad
import Control.Monad.Except
import Data.IORef

import LispVal
import LispPrimitives
import LispParser
import Env

eval :: Env -> LispVal -> IOThrowsError LispVal
eval env val@ (String _) = return val
eval env val@ (Character _) = return val
eval env val@ (Number _) = return val
eval env val@ (Bool _) = return val
eval env (Atom id) = getVar env id
eval env (List [Atom "quote" , val]) = return val
eval env (List [Atom "if" , pred , conseq , alt]) = 
     do result <- eval env pred
        case result of
           Bool False -> eval env alt
           otherwise -> eval env conseq
eval env (List [Atom "set!" , Atom var, form]) =
    eval env form >>= setVar env var
eval env (List [Atom "define" , Atom var , form]) =
    eval env form >>= defineVar env var
eval env (List (Atom "define" : List (Atom var : params) : body)) =
    makeNormalFunc env params body >>= defineVar env var
eval env (List (Atom "define" : DottedList (Atom var : params) varargs : body)) =
    makeVarargs varargs env params body >>= defineVar env var
eval env (List (Atom "lambda" : List params : body)) =
    makeNormalFunc env params body
eval env (List (Atom "lambda" : DottedList params varargs : body)) =
    makeVarargs varargs env params body
eval env (List (Atom "lambda" : varargs@ (Atom _) : body)) =
    makeVarargs varargs env [] body
eval env (List [Atom "load" , String filename]) =
    load filename >>= liftM last . mapM (eval env)
eval env (List (Atom "cond" : clauses)) = evalCondClauses env clauses
eval env (List (Atom "case": key : clauses)) = do keyEval <- eval env key
                                                  res <- evalCaseClauses env keyEval clauses
                                                  return res
eval env (List (function@ (Atom _) : args)) = do
    func <- eval env function
    argVals <- mapM (eval env) args
    apply func argVals
eval env (List lst) = do elems <- mapM (eval env) lst
                         return (List elems)
eval env (DottedList lst el) = do elems <- mapM (eval env) lst
                                  dotElem <- eval env el
                                  return (DottedList elems dotElem)
eval env badForm = throwError $ BadSpecialForm "Unrecognized special form" badForm


evalCondClauses::Env -> [LispVal] -> IOThrowsError LispVal
evalCondClauses env [ List (Atom "else" : elseExprs) ] = evalExprList env elseExprs
evalCondClauses env ( List (testExpr : evalExprs) : clauses ) = do result <- eval env testExpr
                                                                   case result of
                                                                      Bool False -> evalCondClauses env clauses
                                                                      otherwise -> case evalExprs of 
                                                                                [] -> return result
                                                                                otherwise -> evalExprList env evalExprs
evalCondClauses _ (badForm : _) = throwError $ BadSpecialForm "Unrecognized special form" badForm
evalCondClauses _ [] = throwError $ BadSpecialForm "Expected more clauses evaluating to true" (List [])

evalCaseClauses::Env -> LispVal -> [LispVal] -> IOThrowsError LispVal
evalCaseClauses env _ [ List (Atom "else" : elseExprs) ] =  evalExprList env elseExprs
evalCaseClauses env key ( List (List tests : evalExprs) : clauses ) = do check <- сheckCaseCondition env key tests
                                                                         case check of
                                                                             Bool False -> evalCaseClauses env key clauses
                                                                             otherwise -> evalExprList env evalExprs
evalCaseClauses _ _ (badForm : _) =  throwError $ BadSpecialForm "Unrecognized special form" badForm
evalCaseClauses _ _ [] = throwError $ BadSpecialForm "Expected more clauses evaluating to true" (List [])

сheckCaseCondition:: Env -> LispVal -> [LispVal] -> IOThrowsError LispVal
сheckCaseCondition env key [t] = eval env (List [Atom "eqv?", key, t])
сheckCaseCondition env key (t1:tests) = do res <- eval env (List [Atom "eqv?", key, t1])
                                           case res of
                                              Bool True -> return $ Bool True
                                              otherwise -> сheckCaseCondition env key tests
сheckCaseCondition env key [] = throwError $ BadSpecialForm "Expected at least one expression to compare with key" (List [])

evalExprList::Env -> [LispVal] -> IOThrowsError LispVal
evalExprList _ [] = throwError $ BadSpecialForm "Expected at least one expression to evaluate" (List [])
evalExprList env exprList = liftM last $ mapM (eval env) exprList

apply :: LispVal -> [LispVal] -> IOThrowsError LispVal
apply (PrimitiveFunc func) args = liftEither $ func args
apply (IOFunc func) args = func args
apply (Func params varargs body closure) args =
    if num params /= num args && varargs == Nothing
       then throwError $ NumArgs (num params) args
       else (liftIO $ bindVars closure $ zip params args) >>=
          bindVarArgs varargs >>= evalBody
       where remainingArgs = drop (length params) args
             num = toInteger . length
             evalBody env = liftM last $ mapM (eval env) body
             bindVarArgs arg env = case arg of
                Just argName -> liftIO $ bindVars env [(argName, List $ remainingArgs)] 
                Nothing -> return env
apply someVal _ = throwError $ BadSpecialForm "Unrecognized special form" someVal

applyProc :: [LispVal] -> IOThrowsError LispVal
applyProc [func , List args] = apply func args
applyProc (func : args) = apply func args


primitiveBindings :: IO Env
primitiveBindings = nullEnv >>= (flip bindVars $ map (makeFunc IOFunc) ioPrimitives 
    ++ map (makeFunc PrimitiveFunc) primitives)
        where makeFunc constructor (var, func) = (var, constructor func)


makeFunc varargs env params body = return $ Func (map show params) varargs body env
makeNormalFunc = makeFunc Nothing
makeVarargs = makeFunc . Just . show


ioPrimitives :: [( String, [LispVal] -> IOThrowsError LispVal)]
ioPrimitives = [("apply" , applyProc),
                ("open-input-file" , makePort ReadMode),
                ("open-output-file" , makePort WriteMode),
                ("close-input-port" , closePort),
                ("close-output-port" , closePort),
                ("read", readProc),
                ("write", writeProc),
                ("read-contents" , readContents),
                ("read-all" , readAll)]

makePort :: IOMode -> [LispVal] -> IOThrowsError LispVal
makePort mode [String filename] = fmap Port $ catchIOExceptionOrThrow (openFile filename) mode

closePort :: [LispVal] -> IOThrowsError LispVal
closePort [Port port] = (catchIOExceptionOrThrow hClose port) >> (return $ Bool True)
closePort _ = return $ Bool False

readProc :: [LispVal] -> IOThrowsError LispVal
readProc [] = readProc [Port stdin]
readProc [Port port] = (catchIOExceptionOrThrow hGetLine port) >>= liftEither . readExpr
readProc [notPort] = throwError (TypeMismatch "port" notPort)

writeProc :: [LispVal] -> IOThrowsError LispVal
writeProc [obj] = writeProc [obj, Port stdout]
writeProc [obj, Port port] = catchIOExceptionOrThrow (hPrint port) obj >> (return $ Bool True)
writeProc [obj, notPort] = throwError (TypeMismatch "port" notPort)


readContents :: [LispVal] -> IOThrowsError LispVal
readContents [String filename] = fmap String (catchIOExceptionOrThrow readFile filename)
readContents [notString] = throwError (TypeMismatch "string" notString)

load :: String -> IOThrowsError [LispVal]
load filename = (catchIOExceptionOrThrow readFile filename) 
    >>= liftEither . readExprList

readAll :: [LispVal] -> IOThrowsError LispVal
readAll [String filename] = liftM List $ load filename
