{-# LANGUAGE FlexibleContexts#-}
module LispParser(
    readExpr,
    readExprList,
) where

import Data.Char
import Control.Monad
import Control.Monad.Except
import Text.ParserCombinators.Parsec hiding (spaces)
import System.IO

import LispVal

readExpr = readOrThrow (do x <- parseExpr
                           eof <|> spacesEof
                           return x)
readExprList = readOrThrow (endBy parseExpr spaces)

readOrThrow :: Parser a -> String -> ThrowsError a 
readOrThrow parser input = case parse parser "lisp" input of
    Left err -> throwError $ Parser err
    Right val -> return val

symbol :: Parser Char
symbol = oneOf "!$%&|*+-/:<=?>@^_~"

spaces :: Parser ()
spaces = skipMany1 space

--parses spaces and the end of input: needed because of mistake to write '2)' , '+ 2 2 2)' and so on
spacesEof = do spaces
               eof
               return ()


parseString :: Parser LispVal
parseString = do char '"'
                 x <- many $ many1 ( noneOf "\"\\" ) <|> escapedChars
                 char '"'
                 return $ String (concat x)

escapedChars:: Parser String
escapedChars = do char '\\'
                  x <-oneOf "\\\"ntr"
                  case x of
                      '\\' -> do return [x]
                      '"' -> do return [x]
                      't' -> do return "\t"
                      'n' -> do return "\n"
                      'r' -> do return "\r"

parseChar:: Parser LispVal
parseChar =  do try $ string "#\\"
                x <- parseCharName <|> anyChar
                return $ Character x

parseCharName = do x <- parseCaseInsensitiveString "space" <|> parseCaseInsensitiveString "newline"
                   case x of
                       "space" -> do return ' '
                       "newline" -> do return '\n'

parseCaseInsensitiveChar c = do x <- char (toLower c) <|> char (toUpper c)
                                return (toLower x)

parseCaseInsensitiveString s =  try (mapM parseCaseInsensitiveChar s)

parseAtom :: Parser LispVal
parseAtom = do first <- letter <|> symbol
               rest <- many (letter <|> digit <|> symbol)
               let atom = [first] ++ rest
               return $ Atom atom

parseNumber :: Parser LispVal
parseNumber = liftM Number (sign <*> parseDigital)

parseDigital:: Parser Integer
parseDigital = do x <- many1 digit
                  (return . read) x

sign :: Parser (Integer -> Integer)
sign = char '-' *> return negate
   <|> char '+' *> return id
   <|> return id

parseBool :: Parser LispVal
parseBool = do string "#"
               x <- oneOf "tf"
               return $ case x of
                             't' -> Bool True
                             'f' -> Bool False

parseList :: Parser LispVal
parseList = liftM List $ sepBy parseExpr spaces

parseDottedList :: Parser LispVal
parseDottedList = do
        head <- endBy parseExpr spaces
        tail <- char '.' >> spaces >> parseExpr
        return $ DottedList head tail

parseQuoted :: Parser LispVal
parseQuoted = do
     char '\''
     x <- parseExpr
     return $ List [Atom "quote" , x]

parseExpr :: Parser LispVal
parseExpr = parseString
        <|> try parseBool
        <|> try parseChar
        <|> try parseNumber
        <|> try parseAtom
        <|> parseQuoted
        <|> do char '('
               x <- (Text.ParserCombinators.Parsec.try parseList) <|> parseDottedList 
               char ')'
               return x