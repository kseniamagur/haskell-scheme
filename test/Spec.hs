import Test.Hspec
import Text.ParserCombinators.Parsec hiding (spaces)
import System.Environment
import Control.Monad
import Control.Monad.Except


import LispParser
import Eval
import LispVal

main :: IO ()
main = do
    hspec $ describe "Test suit for LipsParser" $ do
-- Test parser
      it "Test parse Atom" $
        readExpr "x&_" `shouldBe` (Right $ Atom "x&_")

      it "Test parse positive Number" $
         readExpr "3" `shouldBe` (Right $ Number 3)

      it "Test parse postive Number with sign" $
         readExpr "+3" `shouldBe` (Right $ Number 3)

      it "Test parse negative Number with sign" $
         readExpr "-3" `shouldBe` (Right $ Number (-3))

      it "Test parse String" $
        readExpr "\"hi, Haskel Tests\"" `shouldBe` (Right $ String "hi, Haskel Tests")

      it "Test parse Bool True" $
        readExpr "#t" `shouldBe` (Right $ Bool True)

      it "Test parse Bool False" $
        readExpr "#f" `shouldBe` (Right $ Bool False)

      it "Test parse List: simple" $
        readExpr "(1 2 3)" `shouldBe` (Right $ List [Number 1, Number 2, Number 3])

      it "Test parse List: primitive operator usage" $
        readExpr "(+ 2 \"3\")" `shouldBe` (Right $ List [Atom "+", Number 2, String "3"])

      it "Test parse List: primitive operator usage for one of elements" $
        readExpr "(1 (+ 2 3) 4)" `shouldBe` (Right $ List [Number 1, List [Atom "+", Number 2, Number 3] , Number 4])

      it "Test parsr List: define new function" $
        readExpr "(define (f x y) (+ x y))" `shouldBe` (Right $ List [Atom "define", List [Atom "f", Atom "x", Atom "y"] , List [Atom "+", Atom "x", Atom "y"] ] )

      it "Test parse Dotted List" $
        readExpr "(1 2 . 3)" `shouldBe` (Right $ DottedList [Number 1, Number 2] (Number 3))

      it "Test parse List : primitive function and dotted list inside" $
        readExpr "(cdr (1 2 . 3))" `shouldBe` (Right $ List [Atom "cdr" , DottedList [Number 1, Number 2] (Number 3)])

      it "Test parse Quoted List: simple" $
        readExpr "'(1 2 3)" `shouldBe` (Right $ List [Atom "quote", List [Number 1, Number 2, Number 3]])
        
      it "Test parse Quoted List: primitive operator usage for one of elements" $
        readExpr "'(1 (+ 2 3) 4)" `shouldBe` (Right $ List [Atom "quote", List [Number 1, List [Atom "+", Number 2, Number 3] , Number 4]])

-- Test evaluation
      describe "Test suit for Eval" $ do
        it "Test eval addition" $ 
          runTestEval Nothing 
           (List [Atom "+", Number 2, String "3"]) 
            `shouldReturn` (Right $ Number 5) 

        it "Test eval string>?" $
          runTestEval Nothing
           (List [Atom "string>?", String "abc", String "aba"])
           `shouldReturn` (Right $ Bool True)

        it "Test eval char?" $
          runTestEval Nothing
           (List [Atom "char?", Character 'a'])
            `shouldReturn` (Right $ Bool True)

        it "Test eval basic_library: list length" $
         runTestEval (Just "test/scheme/lib/basic_library.scm")
          (List [Atom "length", List [String "a", String "b", String "c", String "d"]])
          `shouldReturn`  (Right $ Number 4)

        it "Test eval basic_library: list filter even" $
          runTestEval (Just "test/scheme/lib/basic_library.scm") 
           (List [Atom "filter", Atom "even?", List [Number 1, Number 2, Number 3, Number 4]]) 
            `shouldReturn` (Right $ List[Number 2, Number 4])

        it "Test eval basic_library: list or" $
          runTestEval (Just "test/scheme/lib/basic_library.scm")
           (List [Atom "or", Bool False, Bool False, Bool True])
           `shouldReturn` (Right $ Bool True)

        it "Test eval basic_library: list product" $
         runTestEval (Just "test/scheme/lib/basic_library.scm") 
          (List [Atom "product",Number 1, Number 2, Number 3, Number 4, Number 5])
           `shouldReturn` (Right $ Number 120)


--extract value from lispVal evaluation
runTestEval::Maybe FilePath ->LispVal -> IO (ThrowsError LispVal)
runTestEval libFile lispExpr = runExceptT (testEval libFile lispExpr)

-- evaluate LispVal
testEval::Maybe FilePath -> LispVal -> IOThrowsError LispVal
testEval Nothing lispExpr = do env <- liftIO primitiveBindings
                               res <- eval env lispExpr
                               return res
testEval (Just libFile) lispExpr = do env <- liftIO primitiveBindings
                                      eval env (List [Atom "load" , String libFile])
                                      res <- eval env lispExpr
                                      return res
