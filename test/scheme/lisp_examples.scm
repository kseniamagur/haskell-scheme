(+ 2 "3")

(define (f x y) (+ x y))

(define (counter inc) (lambda (x) (set! inc (+ x inc)) inc))
(define my−count (counter 5))
(my−count 3) -> 8



(load "./test/scheme/basic_library.scm")

(map (curry + 2) '(1 2 3 4)) -> ( 3 4 5 6 )
(filter even? '(1 2 3 4)) -> (2 4)
(filter even? '(1 3 5)) -> ()

(or #f #f #t) -> #t
(or #f #f #f) -> #f

(any? odd? 1 2 3 4) -> #t
(any? odd? 2 4) -> #f

(product 1 2 3 4 5) -> 120

(max 10 30 20) -> 30
(min 10 30 20) -> 10

(length '(1 2 3 4 4 4)) -> 6

(list 1 2 3) -> (1 2 3)

(every? positive? 1 2 3 4) -> #t
(every? positive? 0 1 2 3 4) ->#f

(cond ((> 3 2) #t) ((< 3 2) #f))  

(cond ((> 3 2)) (else 10))  

(case (* 2 3) ((2 3 5 7) "prime") ((1 4 6 8 9) "composite"))

(case 3 ((2 3 5 7) "prime") ((1 4 6 8 9) "composite"))

(case 100 ((2 3 5 7) "prime") ((1 4 6 8 9) "composite"))

(1 2 4)

(if (number? 2) 
     "number" 
     "not a number")

(define (add x y) (+ x y))

(1 2 (+ 2 3) (+ 3 4))

'(1 2 (+ 2 3) (+ 3 4))

'(1 2 (+ 2 3) ,(+ 3 4))