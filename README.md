# haskell-scheme

A basic implementation of Scheme language.

## Start project

```bash
stack setup
stack build
stack exec haskell-scheme-exe
```

## Build with test check

```bash
stack build --test
```

# Run tests

```bash
stack test
```